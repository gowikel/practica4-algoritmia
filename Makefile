.PHONY: clean-all clean-objects

CXX = g++
CXXFLAGS = $(INCLUDE) $(STD) $(OPTIMIZATION) -Wall -O3
INCLUDE = $(HEADER) $(LIBRARY)
HEADER = -I.
LIBRARY = -L.
STD = -std=c++11
OPTIMIZATION = -fbounds-check -fdefault-inline -ffast-math -ffloat-store -fforce-addr -ffunction-cse -finline -finline-functions\
-fmerge-all-constants

practica4: main.o Mochila.o Material.o ListaMateriales.o funcionesAltoNivel.o
	$(CXX) $(CXXFLAGS) -o $@ $^

main.o: main.cpp Mochila.o Material.o
	$(CXX) $(CXXFLAGS) -c -o $@ main.cpp

Material.o: Material.hpp Material.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ Material.cpp

Mochila.o: Mochila.hpp Mochila.cpp Material.o
	$(CXX) $(CXXFLAGS) -c -o $@ Mochila.cpp

ListaMateriales.o: ListaMateriales.cpp ListaMateriales.hpp Material.o
	$(CXX) $(CXXFLAGS) -c -o $@ ListaMateriales.cpp
	
funcionesAltoNivel.o: funcionesAltoNivel.cpp funcionesAltoNivel.hpp
	$(CXX) $(CXXFLAGS) -c -o $@ funcionesAltoNivel.cpp

clean-objects:
	-rm *.o

clean-all: clean-objects
	-rm practica4